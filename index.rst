.. Tránsito Normas documentation master file, created by
   sphinx-quickstart on Wed Jul  8 11:56:03 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Normas de tránsito
==================

.. toctree::
   :maxdepth: 2
   :caption: Decretos supremos

   DS 024-2002-MTC                      <decsup/ds-024-2002-mtc>

.. toctree::
   :maxdepth: 2
   :caption: Resoluciones ministeriales

   RM 0338-2020-MTC-01-02               <resmin/rm-0338-2020-mtc-01-02>



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
